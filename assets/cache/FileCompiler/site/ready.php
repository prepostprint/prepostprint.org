<?php
function multiexplode($delimiters,$data) {
	$MakeReady = str_replace($delimiters, $delimiters[0], $data);
	$Return    = explode($delimiters[0], $MakeReady);
	return  $Return;
}
$forms->addHookBefore('FormBuilderProcessor::processInputDone', function($e) {
  $form = $e->arguments(0);
  if($form->name == 'new_submission') {

		// Normal fields
		$country = $form->getChildByName('country');
    $countryTitle = $country->attr('value');
		$countryName = \ProcessWire\wire('sanitizer')->pageName($countryTitle,true);
    $countryTofind = \ProcessWire\wire('pages')->find("name=$countryName");
    if (count($countryTofind) == 0) {
      $page = new \ProcessWire\Page();
      $page->parent = "/config/country/";
      $page->template = "country";
      $page->title = $countryTitle;
      $page->save();
    }


		$publisher = $form->getChildByName('publisher');
    $publisherTitle = $publisher->attr('value');
		$publisherName = \ProcessWire\wire('sanitizer')->pageName($publisherTitle,true);
    $publisherTofind = \ProcessWire\wire('pages')->find("name=$publisherName");
    if (count($publisherTofind) == 0) {
      $page = new \ProcessWire\Page();
      $page->parent = "/config/publisher/";
      $page->template = "publisher";
      $page->title = $publisherTitle;
      $page->save();
    }


		$printer = $form->getChildByName('printer');
    $printerTitle = $printer->attr('value');
		$printerName = \ProcessWire\wire('sanitizer')->pageName($printerTitle,true);
    $printerTofind = \ProcessWire\wire('pages')->find("name=$printerName");
    if (count($printerTofind) == 0) {
      $page = new \ProcessWire\Page();
      $page->parent = "/config/printer/";
      $page->template = "printer";
      $page->title = $printerTitle;
      $page->save();
    }


    $format = $form->getChildByName('format');
    $formatTitle = $format->attr('value');
		$formatName = \ProcessWire\wire('sanitizer')->pageName($formatTitle,true);
    $formatTofind = \ProcessWire\wire('pages')->find("name=$formatName");
    if (count($formatTofind) == 0) {
      $page = new \ProcessWire\Page();
      $page->parent = "/config/format/";
      $page->template = "format";
      $page->title = $formatTitle;
      $page->save();
    }
  }
});

// Handle multi-page fields, and multiple entry (with , and / separators in form input)

$forms->addHookBefore('FormBuilderProcessor::savePageReady', function($e) {
	$page = $e->arguments(0);
	$data = $e->arguments(1);
	$page->of(false);
  $processor = $e->object; // FormBuilderProcessor instance
	$form = $processor->formName;
	if ($form == 'new_submission') {
		$designerTitle = $data['designers'];
		$designerList = multiexplode(array(",",", ","/"," / "),$designerTitle);
		foreach ($designerList as $designer) {
			$designerName = \ProcessWire\wire('sanitizer')->pageName($designer,true);
			$designerTofind = \ProcessWire\wire('pages')->find("name=$designerName");
			if (count($designerTofind) == 0) {
				$newPage = new \ProcessWire\Page();
				$newPage->parent = "/config/designer/";
				$newPage->template = "designer";
				$newPage->title = $designer;
				$newPage->save();
				$designerTofind = $newPage;
			}
			$page->designers->add($designerTofind);
		}

		$softTitle = $data['software'];
		$softList = multiexplode(array(",",", ","/"," / "),$softTitle);
		foreach ($softList as $soft) {
			$softName = \ProcessWire\wire('sanitizer')->pageName($soft,true);
			$softTofind = \ProcessWire\wire('pages')->find("name=$softName");
			// $page->desc = count($softTofind);

			if (count($softTofind) == 0) {
				$newPage = new \ProcessWire\Page();
				$newPage->parent = "/config/software/";
				$newPage->template = "software";
				$newPage->title = $soft;
				$newPage->save();
				$softTofind = $newPage;
			}
			$page->software->add($softTofind);
		}
	  $e->arguments(1, $data);
		$page->save();
	}
});
?>
