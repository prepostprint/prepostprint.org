  <?php if ($page->template == "selection"): ?>
    <script src="<?php echo $config->urls->templates?>js/jquery.js"></script>
    <script src="<?php echo $config->urls->templates?>js/table.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/table.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/selection.css" />
    <script src="<?php echo $config->urls->templates?>js/magnific-popup.js"></script>
    <script src="<?php echo $config->urls->templates?>js/selection.js"></script>
  <?php elseif ($page->template == "home"): ?>
    <script src="<?php echo $config->urls->templates?>js/home.js"></script>
  <?php elseif ($page->template == "ressources"): ?>
    <?php echo $form->styles; ?>
    <?php echo $form->scripts; ?>
    <script src="<?php echo $config->urls->templates?>js/ressources.js"></script>
  <?php elseif ($page->name == "submit"): ?>
    <?php echo $form->styles; ?>
    <?php echo $form->scripts; ?>
  <?php endif ?>
  <script src="<?php echo $config->urls->templates?>js/main.js"></script>
  </body>
</html>
