<?php
$items = $pages->find('template=item, sort=-sort');
echo "<table id='table' class='box b23'>";
echo "<thead><tr>";
echo "<th class='image'>Image</th>";
echo "<th class='title'>Title</th>";
echo "<th class='date'>Made with</th>";
echo "<th class='tools'>Date</th>";
echo "<th class='designer'>Designer(s)</th>";
echo "</tr></thead>";
echo "<tbody>";
$rowId = 0;
foreach ($items as $i) {
  $name = $i->title;
  $desc = $i->desc;
  $imgUrl = $i->images->first->width(600)->url;
  $category = $i->category->title;
  $binding = $i->binding->title;
  $country = $i->country->title;
  $date = $i->publication_date;
  $format = $i->format->title;
  $link = $i->link;
  $printer = $i->printer->title;
  $printing_technique = $i->printing_technique->title;
  if ($i->publisher) {
    $publisher = $i->publisher->title;
  }
  echo "<tr class='item' id='$rowId'>";
  echo "<td class='image'><img src='$imgUrl'/></td>";
  echo "<td class='title'>$name</td>";
  echo "<td class='tools'>";
  foreach ($i->software as $soft) {
    echo $soft->title." ";
  }
  echo "</td>";
  echo "<td class='date'>$date</td>";
  echo "<td class='designer'>";
  foreach ($i->designers as $designer) {
    echo $designer->title." ";
  }
  echo "</td>";
  // echo "<td class='category'>$category</td>";
  $rowId++;
}
echo "</tbody>";
echo "</table>";

$itemId = 0;
foreach ($items as $i) {
  $name = $i->title;
  $desc = $i->desc;
  $images = $i->images;
  $category = $i->category->title;
  $binding = $i->binding->title;
  $copies = $i->copies;
  $country = $i->country->title;
  $format = $i->format->title;
  $date = $i->publication_date;
  $link = $i->link;
  $printer = $i->printer->title;
  $printing_technique = $i->printing_technique->title;
  if ($i->publisher) {
    $publisher = $i->publisher->title;
  }
    echo "<div class='detail-box box b13' id='selected-$itemId'>";
    echo "<div class='detail-item title'><h3>Title</h3>$name</div>";
    echo "<div class='detail-item designer'><h3>Designer(s)</h3>";
    foreach ($i->designers as $designer) {
      if ($designer->link) {
        echo "<a href='".$designer->link."'>".$designer->title."</a>";
      }else {
        echo $designer->title." ";
      }
      if($designer->editable()) echo " (<a href='$designer->editURL'>Edit</a>)";
    }
    echo "</div>";
    echo "<div class='detail-item tools'><h3>Tool(s)</h3>";
    foreach ($i->software as $soft) {
      echo $soft->title." ";
    }
    echo "</div>";
    echo "<div class='detail-item binding'><h3>Binding</h3>$binding</div>";
    echo "<div class='detail-item format'><h3>Format</h3>$format</div>";
    if ($link != "") {
      echo "<div class='detail-item link'><h3>Link</h3><a target='_blank' href='$link'>Visit</a></div>";
    }
    echo "<div class='detail-item printer'><h3>Printer</h3>$printer</div>";
    echo "<div class='detail-item copies'><h3>Print run</h3>$copies copies</div>";
    echo "<div class='detail-item copies'><h3>Date</h3>$date</div>";
    echo "<div class='detail-item publisher'><h3>Publisher</h3>$publisher</div>";
    echo "<div class='detail-item category'><h3>Category</h3>$category</div>";
    echo "<div class='detail-item country'><h3>Country</h3>$country</div>";
    echo "<div class='detail-item printing_technique'><h3>Printing technique</h3>$printing_technique</div>";
    echo "<div class='detail-item desc'><h3>Description</h3>$desc</div>";
    echo "<div class='detail-item image-list'>";
    foreach ($i->images as $image) {
      $url = $image->url;
      echo "<a href='$url'>";
      echo "<img src='$url'>";
      echo "</a>";
    }
    echo "</div>";
    echo "<div class='detail-item edit'>";
    if($i->editable()) echo "<a href='$i->editURL'>Edit</a>";
    echo "</div>";
    echo "</div>";
  $itemId++;
}
