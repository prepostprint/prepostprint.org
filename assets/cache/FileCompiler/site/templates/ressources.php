<?php
  /**
   * Ressources template
   *
   */
 $form = $forms->render('new_ressource');
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/header.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
  <main>
		<h1><?php echo $page->title; ?></h1>
    <section class="ressources container nopadding-side">
     <div class="list-ressource box b23">
       <section id="filter-btns" class="box container padding-bottom">
         <?php
         $tags = $pages->find('template=ressource_tag');
         echo "<button class='btn button active' data-filter='all'>All</button>";
         foreach ($tags as $tag) {
           echo "<button class='btn button' data-filter='".$tag->name."'>".$tag->title."</button>";
          }
         ?>
       </section>
       <?php
       $ressources = $pages->find('template=ressource, sort=-created');
       foreach ($ressources as $r) {
         $name = $r->title;
         $url = $r->link;
         $desc = $r->desc;
         $tagList = "";
         foreach ($r->ressource_tag as $tag) {
           $title = $tag->name;
           $tagList .= " $title";
         }
         echo "<div class='ressource container nopadding-side $tagList'>";
         echo "<div class='box b13'><p class='title'><a href='$url'>$name</a></p>";
         echo "<p class='link'><a href='$url'>$url</a></p></div>";
         echo "<div class='desc box b13'>$desc</div>";
         echo "<div class='tag-list box b13'>";
         foreach ($r->ressource_tag as $tag) {
           $title = $tag->title;
           echo "<span class='tag'>$title</span>";
         }
         if($r->editable()) echo "<a href='$r->editURL'>Edit</a>";
         echo "</div>";
         echo "</div>";
       }
       ?>
     </div>
     <div class="add-ressource box b13">
       <h2>Add a ressource</h2>
       <?php echo $form; ?>
     </div>
   </section>
  </main>
<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/footer.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
