	<?php
	if ($page->name == "submit") {
 		$form = $forms->render('new_submission');
	}

 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/header.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
	<main>
		<h1><?php echo $page->title; ?></h1>
		<section class="container nopadding-side">

			<?php
	    $boxes = $page->content_box;
	    foreach ($boxes as $box) {
				if ($page->name == "submit") {
					echo "<section class='b13 text box'>";
				}else {
					echo "<section class='b12 text box'>";
				}
        echo "<h2>".$box->title."</h2>";
        if (count($box->images)>0) {
          $url = $box->images->first->url;
          echo "<img src='$url'>";
        }
        echo $box->desc;
        echo "</section>";
	    }
	    ?>

			<?php if ($page->name == "submit"): ?>
				<div class="box b23">
					<?php echo $form; ?>
				</div>
			<?php endif; ?>
		</section>
		<?php if($page->editable()) echo "<p><a href='$page->editURL'>Edit</a></p>"; ?>
	</main>
	<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/footer.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
