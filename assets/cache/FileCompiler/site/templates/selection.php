<?php
  /**
   * Selection template
   *
   */
 include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/header.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
  <main>
    <h1><?php echo $page->title; ?></h1>
    <section class="image-board container nopadding-side">
      <?php
      $items = $pages->find('template=item, sort=-sort');
      $count = 0;
      foreach ($items as $i) {
        echo "<div id='image-list-$count' class='image-list'>";
        foreach ($i->images as $image) {
          $url = $image->url;
          echo "<a href='$url'>";
          echo "<img src='$url'>";
          echo "</a>";
        }
        echo "</div>";
        $count++;
      }
      ?>
    </section>
    <section class="container tableContainer nopadding">
      <?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/table.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true)))?>
    </section>
  </main>

<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/footer.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
