<?php
  /**
   * RSS template
   *
   */
// retrieve the RSS module

$rss = $modules->get("MarkupRSS");

// configure the feed. see the actual module file for more optional config options.
$rss->title = "PrePostPrint Feed";
$rss->description = "The most recent updates on PrePostPrint";

// find the pages you want to appear in the feed.
// this can be any group of pages returned by $pages->find() or $page->children(), etc.
$selectionPath="/selection/";
$ressourcePath="/ressources/";
// $items = $pages->find("parent=$ressourcePath");
$items = $pages->find("template=item");
// $items = $pages->get("/ressources/")->children();

// foreach ($items as $item) {
//   echo $item->title;
// }
// send the output of the RSS feed, and you are done
$rss->render($items);
