<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/header.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
  <main class="full">

<?php
  $i = $page;
  $name = $i->title;
  $desc = $i->desc;
  $category = $i->category->title;
  $binding = $i->binding->title;
  $copies = $i->copies;
  $country = $i->country->title;
  $format = $i->format->title;
  $link = $i->link;
  $printer = $i->printer->title;
  $printing_technique = $i->printing_technique->title;
  $publisher = $i->publisher->title;
  echo "<div class='image-list-single'>";
  foreach ($i->images as $image) {
    $url = $image->url;
    echo "<a href='$url'>";
    echo "<img src='$url'>";
    echo "</a>";
  }
  echo "</div>";

    echo "<div class=' box b12'>";
    echo "<div class='detail-item title'><h3>Title</h3>$name</div>";
    echo "<div class='detail-item designer'><h3>Designer(s)</h3>";
    foreach ($i->designers as $designer) {
      if ($designer->link) {
        echo "<a href='".$designer->link."'>".$designer->title."</a>";
      }else {
        echo $designer->title;
      }
      if($designer->editable()) echo " (<a href='$designer->editURL'>Edit</a>)";
    }
    echo "</div>";
    echo "<div class='detail-item tools'><h3>Tool(s)</h3>";
    foreach ($i->software as $soft) {
      echo $soft->title;
    }
    echo "</div>";
    echo "<div class='detail-item binding'><h3>Binding</h3>$binding</div>";
    echo "<div class='detail-item format'><h3>Format</h3>$format</div>";
    echo "<div class='detail-item link'><h3>Link</h3>$link</div>";
    echo "<div class='detail-item printer'><h3>Printer</h3>$printer</div>";
    echo "<div class='detail-item copies'><h3>Print run</h3>$copies copies</div>";
    echo "<div class='detail-item publisher'><h3>Publisher</h3>$publisher</div>";
    echo "<div class='detail-item category'><h3>Category</h3>$category</div>";
    echo "<div class='detail-item country'><h3>Country</h3>$country</div>";
    echo "<div class='detail-item printing_technique'><h3>Printing technique</h3>$printing_technique</div>";
    echo "<div class='detail-item desc'><h3>Description</h3>$desc</div>";
    echo "<div class='detail-item edit'>";
    if($i->editable()) echo "<a href='$i->editURL'>Edit</a>";
    echo "</div>";
    echo "</div>";

 ?>
  </main>

<?php include(\ProcessWire\wire('files')->compile(\ProcessWire\wire("config")->paths->root . "site/templates/footer.inc",array('includes'=>true,'namespace'=>true,'modules'=>true,'skipIfNamespace'=>true))); ?>
