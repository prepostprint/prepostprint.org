<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="<?php echo $config->urls->templates?>img/favicon.png">
  	<meta property="og:image" content="<?php echo $config->urls->templates?>img/icon.png" />
    <title><?php echo $page->title; ?> - PrePostPrint</title>
    <link rel="alternate" type="application/rss+xml" title="PrePostPrint RSS feed" href="https://prepostprint.org/rss/" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>fonts/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>css/main.css" />
    <meta name="description" content="PrePostPrint highlights experimental publications made with free software" />
    <meta name="keywords" content="free software, graphic design, css print, archive, publication, experimental"/>
    <meta name="twitter:card" content="summary" />
    <meta property="og:title" content="PrePostPrint" />
    <meta name="twitter:title" content="PrePostPrint" />
    <meta property="og:url" content="https://prepostprint.org/"/>
    <meta property="twitter:url" content="http://prepostprint.org/"/>
    <meta property="og:image" content="http://prepostprint.org/site/templates/img/og.png"/>
    <meta name="viewport" content="width=device-width">
  </head>
  <body data-page="<?php echo $page->name ?>">
  <header class="container">
    <h1 class="box b23"><a href="<?php echo $config->urls->root?>"><span class="p1">Pre</span><span class="p2">Post</span><span class="p3">Print</span></a></h1>
    <select class="box select b13" onchange="location = this.value;">
      <?php
      echo "<option data-page='home' value='".$pages->get('/')->url."'>Home</option>";
      echo "<option data-page='selection' value='".$pages->get('/selection')->url."'>PPP Selection</option>";
      echo "<option data-page='resources' value='".$pages->get('/resources')->url."'>Resources</option>";
      echo "<option data-page='faq' value='".$pages->get('/faq')->url."'>F.A.Q.</option>";
      echo "<option data-page='submit' value='".$pages->get('/submit')->url."'>Submit</option>";
      echo "<option data-page='contact' value='".$pages->get('/contact')->url."'>Contact</option>";
      ?>
    </select>
  </header>
