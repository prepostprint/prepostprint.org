
var itemLenght = document.querySelectorAll('.image-list').length
var autoplay = setInterval(function(){
  var r = Math.round(Math.random(itemLenght))
  selectItem(r)
},5000)

var table = $('#table').DataTable({
    language: {
      searchPlaceholder: "Search the entries",
      search:'',
    },
    paging:   false,
    info:false
  });

  $('#table tbody').on( 'click', 'tr', function () {
    $('#table tr').removeClass('selected');
    $(this).addClass('selected');
    var id = table.row(this).id()
    openSelection(id)
    clearInterval(autoplay)
  } );

// startup
function selectItem(id){
  openSelection(id)
  $('#table tr').removeClass('selected');
  $('#table tr#'+id).addClass('selected');
}
selectItem(0)

// Table and image display

function openSelection(id){
  var picked = document.querySelectorAll('.picked')
  if (picked) {
    for (var i = 0; i < picked.length; i++) {
      picked[i].classList.remove('picked')
    }
  }
  var box = document.getElementById('selected-'+id+'')
  var imgList = document.getElementById('image-list-'+id+'')
  box.classList.add('picked')
  imgList.classList.add('picked')
}
var imgToMess = document.querySelectorAll('.image-board .image-list a')
messyGrid(imgToMess, 60, 40)

// Thumbnail grid

function messyGrid(items, offsetVert, offsetHor){
  for (var i = 0; i < items.length; i++) {
    var ov = offsetVert
    var oh = offsetHor
    var rleft = Math.round(Math.random() * (oh + oh) - oh)
    var rtop  = Math.round(Math.random() * (ov + ov) - ov)
    items[i].setAttribute('style','top:'+rtop+'px; left:'+rleft+'px;')
  }
}

// image Popup

$('.image-list').each(function() {
	$(this).magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: true,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
    callbacks: {
      open: function() {
        clearInterval(autoplay)
      },
    },
    image: {
			verticalFit: true,
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300,
    }
   });
})
