<?php
  /**
   * Home template
   *
   */
  include("./header.inc"); ?>
  <main class="container">
    <section class="ppp-intro-image box b12">
      <img class="logo logo-m" src="<?php echo $config->urls->templates ?>/img/ppp-m.svg" alt="">
      <img class="logo logo-c" src="<?php echo $config->urls->templates ?>/img/ppp-c.svg" alt="">
      <img class="logo logo-y" src="<?php echo $config->urls->templates ?>/img/ppp-y.svg" alt="">
      <img class="logo logo-k" src="<?php echo $config->urls->templates ?>/img/ppp.svg" alt="">
    </section>

    <?php
    $boxes = $page->content_box;
    echo "<section class='home-box-list box b12 container'>";

    foreach ($boxes as $box) {
      if ($box !== $boxes[0]) {
        if (isset($box->boxurl->url)) {
          $boxUrl = $box->boxurl->url;
          echo "<a href='$boxUrl' class='home-box b11 box'>";
        }else {
          echo "<div class='home-box b11 box'>";
        }
        echo "<h2>".$box->title."</h2>";
        // if ($box->images) {
        //   $url = $box->images->first->url;
        //   echo "<img src='$url'>";
        // }
        echo $box->desc;
        if (isset($box->boxurl->url)) {
          echo "</a>";
        }else {
          echo "</div>";
        }
      }
    }
    echo "</section>";
    ?>
  </main>

<?php  include("./footer.inc"); ?>
